﻿var gridApp = angular.module('hw_grid', []);
gridApp.controller('gridCtrl', [
    '$scope', '$http', function (scope, http) {
        //http.get('sampleJSON.txt').success(function (data) {

        //    scope.persons = data;
        //});
        scope.persons =  [
        {
            "firstName": "John",
            "lastName": "Smith",
            "age": 25,
            "address":
            {
                "streetAddress": "21 2nd Street",
                "city": "New York",
                "state": "NY",
                "postalCode": "10021"
            },
            "phoneNumber":
            [
                {
                    "type": "home",
                    "number": "212 555-1234"
                },
                {
                    "type": "fax",
                    "number": "646 555-4567"
                }
            ]
        },
        {
            "firstName": "Simona",
            "lastName": "Morasca",
            "age": 22,
            "address":
            {
                "streetAddress": "3 Mcauley Dr",
                "city": "Ashland",
                "state": "OH",
                "postalCode": "44805"
            },
            "phoneNumber":
                [
                    {
                        "type": "home",
                        "number": "419-503-2484"
                    },
                    {
                        "type": "fax",
                        "number": "419-800-6759"
                    }
                ]
        },
        {
            "firstName": "Josephine",
            "lastName": "Darakjy",
            "age": 33,
            "address":
            {
                "streetAddress": "4 B Blue Ridge Blvd",
                "city": "Brighton",
                "state": "MI",
                "postalCode": "48116"
            },
            "phoneNumber":
                [
                    {
                        "type": "home",
                        "number": "973-605-6492"
                    },
                    {
                        "type": "fax",
                        "number": "602-919-4211"
                    }
                ]
        }
    ];
    
        scope.reverse = true;
        scope.showDetails = false;
        scope.toggleText = 'Show Lesson Details.';


        scope.resetAll = function () {
          
            scope.Header = ['', '', '',''];
         
        }







        scope.sort = function (sortBy) {
        
            scope.resetAll();
           
            scope.columnToOrder = sortBy;
           
            //$Filter - Standard Service
         
         //   scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);
            
            if (scope.reverse) iconName = 'glyphicon glyphicon-chevron-up';
           
              else iconName = 'glyphicon glyphicon-chevron-down';
            
            if (sortBy === 'firstName') {
                
                scope.Header[0] = iconName;
                
            } else if (sortBy === 'lastName') {
            
                scope.Header[1] = iconName;
               
            } else if (sortBy === 'age') {
               
                scope.Header[2] = iconName;
                
            }
            if (sortBy === 'address.city') {
                scope.Header[3] = iconName;
            }

            scope.reverse = !scope.reverse;
            
          
       
        };


        scope.$watch('showDetails', function () {
            scope.toggleText = scope.showDetails ? 'Hide Lesson Details.' : 'Show Lesson Details.';
        });

        
 
     


        //By Default sort ny firstName
     
        scope.sort('firstName');











    }
]);